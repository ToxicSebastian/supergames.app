import React from 'react';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        email: "",
        password: ""
    }

    handleEmail = (email) => {
        this.setState({ email })
    }

    handlePassword = (password) => {
        this.setState({ password })
    }

    handleSubmit = () => {
        console.log(this.state.email, this.state.password);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <input type="email" onChange={(email) => this.handleEmail(email)} /> <br />
                <input type="password" onChange={(pass) => this.handlePassword(pass)} /> <br />
                <input type="submit" value="Submit" />
            </form>
        );
    }
}
