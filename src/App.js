import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './App.css'

class App extends React.Component {

  state = {
    isLoggedIn: 0,
    activeTab: 0,
    activeTabName: ''
  }

  componentDidMount() {
    this.setState({ activeTab: 0 })
  }

  render() {
    return (
      <div className="App">

        <Tabs>
          <TabList className={"tabList"}>
            <Tab className="tab" onClick={() => this.setState({ activeTab: 0 })}>Hem</Tab>
            <Tab className="tab" onClick={() => this.setState({ activeTab: 1 })}>Mina spel</Tab>
            <Tab className="tab" onClick={() => this.setState({ activeTab: 2 })}>Mina vänner</Tab>
          </TabList>

          <TabPanel className="tab-panel">
            <p>Hem</p>
          </TabPanel>
          <TabPanel className="tab-panel">
            <p>Mina Spel</p>
          </TabPanel>
          <TabPanel className="tab-panel">
            <p>Mina vänner</p>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default App;
